var _a = require('electron'), app = _a.app, BrowserWindow = _a.BrowserWindow;
var path = require('path');
app.whenReady().then(function () {
    var win = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            preload: __dirname + '/preload.js'
        }
    });
    var dev = false;
    if (dev) {
        win.loadFile('http://localhost:3000');
    }
    else {
        win.loadURL("file://".concat(path.join(__dirname, '../build/index.html')));
        win.loadFile("".concat(path.join(__dirname, '../build/index.html')));
    }
    win.webContents.openDevTools();
});
app.on('window-all-closed', function () {
    app.quit();
});
